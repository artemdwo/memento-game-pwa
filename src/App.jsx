import shuffle from "./utils/shuffle";
import Card from "./components/Card";
import Header from "./components/Header";
import { useState, useEffect } from "react";
import useAppBadge from "./hooks/useAppBadge";

function App() {
  const [cards, setCards] = useState(shuffle); // Cards Array based on /assets
  const [pickOne, setPickOne] = useState(null); // Select First card
  const [pickTwo, setPickTwo] = useState(null); // Select Second card
  const [disabled, setDisabled] = useState(false); // Handle Delay
  const [wins, setWins] = useState(0); // Count wins
  const [setBadge, clearBadge] = useAppBadge(); // Handle set and clear app badge

  // handle click on a card (selection)
  const handleClick = (card) => {
    if (!disabled) {
      pickOne ? setPickTwo(card) : setPickOne(card);
    }
  };

  // handle selection turns
  const handleTurns = () => {
    setPickOne(null);
    setPickTwo(null);
    setDisabled(false);
  };

  // Handle start of a New game
  const handleNewGame = () => {
    clearBadge();
    setWins(0);
    handleTurns();
    setCards(shuffle);
  };

  // Use effect for selection and match handling
  useEffect(() => {
    let pickTimer;

    // When TWO cards have been clicked
    if (pickOne && pickTwo) {
      // Check wether both selected images do match
      if (pickOne.image === pickTwo.image) {
        setCards((preCards) => {
          return preCards.map((card) => {
            if (card.image === pickOne.image) {
              // If THE MATCH
              // Reflect the match in card properties
              return { ...card, matched: true };
            } else {
              // If NO MATCH
              return card;
            }
          });
        });
        handleTurns();
      } else {
        // Pause
        setDisabled(true);
        // and Delay
        pickTimer = setTimeout(() => {
          handleTurns();
        }, 1000);
      }
    }

    return () => {
      clearTimeout(pickTimer);
    };
  }, [cards, pickOne, pickTwo]);

  // Use effect for the WIN when all cards got matched
  useEffect(() => {
    const checkWin = cards.filter((card) => !card.matched);

    if (cards.length && checkWin.length < 1) {
      setWins(wins + 1);
      handleTurns();
      setCards(shuffle);
      setBadge();
    }
  }, [cards, wins]);

  return (
    <>
      <Header handleNewGame={handleNewGame} wins={wins} />

      <div className="grid">
        {cards.map((card) => {
          const { image, id, matched } = card;

          return (
            <Card
              key={id}
              image={image}
              selected={card === pickOne || card === pickTwo || matched}
              onClick={() => handleClick(card)}
            />
          );
        })}
      </div>
    </>
  );
}

export default App;
